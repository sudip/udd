#!/usr/bin/env python

"""
This script imports upstream metadata like bibliographic references of
scientific programs, registry entries of such programs and other upstream
metadata.  The definition of metadata is at

   https://wiki.debian.org/UpstreamMetadata

The data that are consumed here are gathered once per day from Salsa by
the script

   https://salsa.debian.org/blends-team/website/blob/master/misc/machine_readable/fetch-machine-readable_salsa.py

which creates an archive that can be downloaded here

   http://blends.debian.net/_machine-readable/machine-readable.tar.xz
"""

from sys import stderr, exit
from os import listdir, unlink, rename, access, X_OK
from os.path import isfile
from fnmatch import fnmatch
import yaml
from psycopg2 import IntegrityError, InternalError
import re
import logging
import logging.handlers
from subprocess import Popen, PIPE
import json
from types import *

debug=0

def get_gatherer(connection, config, source):
  return bibref_gatherer(connection, config, source)

def rm_f(file):
  try:
    unlink(file)
  except OSError:
    pass

def cleanup_tex_logs(basetexfile):
  rm_f(basetexfile+'.aux')
  rm_f(basetexfile+'.bbl')
  rm_f(basetexfile+'.blg')
  rm_f(basetexfile+'.log')

# seek for authors separated by ',' rather than by ' and '
seek_broken_authors_re = re.compile('^[^\s^,]+\s+[^\s^,]+\s*,\s*[^\s^,]+\s+[^\s^,]')

def open_tex_process(texexe, basetexfile):
  if texexe == 'pdflatex':
    ptex = Popen(['pdflatex', '-interaction=batchmode', basetexfile], shell=False, stdout=PIPE)
  elif texexe == 'bibtex':
    ptex = Popen(['bibtex', basetexfile], shell=False, stdout=PIPE)
  else:
    return(False, 'Wrong exe: '+texexe)
  errstring=""
  if ptex.wait():
    if texexe == 'pdflatex':
      for logrow in ptex.communicate()[0].splitlines():
        if logrow.startswith('!'):
          errstring += logrow
      return(False, errstring)
    else:
      for logrow in ptex.communicate()[0].splitlines():
        if logrow.startswith('This is BibTeX'):
          continue
        errstring += logrow + '\n'
      return(True, errstring)
  return(True, errstring)

known_keys       = ('Archive',
                    'ASCL-Id',
                    'Bug-Database',
                    'Bug-Submit',
                    'Cite-As',
                    'Changelog',
                    'Contact',
                    'CPE',
                    # 'CRAN',
                    'Documentation',
                    'Donation',
                    # 'Download',
                    'FAQ',
                    'Funding',
                    'Gallery',
                    # 'Help',
                    'Name',
                    'Other-References',
                    'Reference',
                    'Registration',
                    'Registry',
                    'Repository',
                    'Repository-Browse',
                    'Screenshots',
                    'Security-Contact',
                    'Webservice')

orphaned_keys    = ('Homepage',
                    'Watch')

class upstream_reader():
  """
  Read references from single debian/upstream/metadata file
  For the definition of this file see
     https://wiki.debian.org/UpstreamMetadata
  """

  def __init__(self, ufile, source, log, blend):
    uf = open(ufile)
    self.source     = source
    self.references = None
    self.fields     = None
    self.log        = log
    self.ubibrefs   = []
    self.ubibrefsinglelist = []
    self.umetadata  = []
    self.blend      = blend
    self.registry   = None

    log.debug("Reading %s of source '%s' of %s" % (ufile, source, blend))
    try:
      self.fields = yaml.safe_load(uf.read())
    except yaml.scanner.ScannerError, err:
      self.log.error("Scanner error in file %s of %s: %s" % (ufile, blend, str(err)))
      return
    except yaml.parser.ParserError, err:
      self.log.error("Parser error in file %s of %s: %s" % (ufile, blend, str(err)))
      return
    except yaml.reader.ReaderError, err:
      self.log.error("Encoding problem in file %s of %s: %s" % (ufile, blend, str(err)))
    if self.fields is None:
      self.log.error("Empty metadata file %s of %s" % (ufile, blend))
      return
    if 'Reference' in self.fields:
      self.references=self.fields['Reference']
    if 'Registry' in self.fields:
      self.registry=self.fields['Registry']
      log.debug("Registry data found for source '%s' of %s: registry = %s" % (source, blend, str(self.registry)))
    else:
      log.debug("No registry data found for source '%s' of %s" % (source, blend))
    warn_keys = []
    for key in self.fields.keys():
      if key not in known_keys:
        warn_keys.append(key)
        continue
      if key in ('Reference', 'Registry'):
        continue
      else:
        if self.fields[key] is not None:
          if type(self.fields[key]) is str or type(self.fields[key]) is unicode:
            ukeyvalue={}
            ukeyvalue['source']  = self.source
            ukeyvalue['key']     = key
            ukeyvalue['value']   = self.fields[key]
            self.umetadata.append(ukeyvalue)
          elif type(self.fields[key]) is list:
            ukeyvalue={}
            ukeyvalue['source']  = self.source
            ukeyvalue['key']     = key
            ukeyvalue['value']   = self.fields[key][0]
            # FIXME: We can only store unique strings in current UDD data structure - so we just take the first value for the moment
            self.log.warning("Only first element of list in file %s of %s for key '%s' is used: %s" % (ufile, blend, key, ukeyvalue['value']))
            self.umetadata.append(ukeyvalue)
          elif type(self.fields[key]) is float and key == 'ASCL-Id':
            ukeyvalue={}
            ukeyvalue['source']  = self.source
            ukeyvalue['key']     = key
            ukeyvalue['value']   = str(self.fields[key])
            self.log.warning("Float value for %s in file %s of %s is converted to string: %s" % (key, ufile, blend, ukeyvalue['value']))
            self.umetadata.append(ukeyvalue)
          else:
            self.log.error("Type problem in file %s of %s: %s is not allowed for key %s" % (ufile, blend, type(self.fields[key]), key))
        else:
          self.log.info("Empty value for %s in file %s of %s" % (key, ufile, blend))
    if len(warn_keys) > 0:
      self.log.error("The following keys in file %s of %s of %s are unknown: %s" % (ufile, source, blend, str(warn_keys)))
    return

  def parse(self):
    if isinstance(self.references, list):
      # upstream file contains more than one reference
      rank={}      # record different ranks per binary package
      rank[''] = 0 # default is to have no specific Debian package which is marked by '' in the package column
      refid = 0
      for singleref in self.references:
        try:
          singleref['refid'] = refid # refid is not used currently but might make sense to identify references internally
        except:
          self.log.error("Severely broken reference for source %s of %s - ignoring this reference" % (self.source, self.blend))
          self.references = None
          return
        singleref['package'] = ''
        package_found = False
        for r in singleref.keys():
          key = r.lower()
          if key != 'debian-package':
            continue
          # self.log.warning("Source package '%s' has key 'debian-package'", self.source)
          pkg = singleref['package'] = singleref[r]
          package_found = True
          if rank.has_key(pkg):
            rank[pkg] += 1
          else:
            rank[pkg]  = 0
          singleref['rank'] = rank[pkg]
        if not package_found:
          singleref['rank'] = rank['']
          rank[''] += 1
      for singleref in self.references:
        self.setref(singleref, singleref['package'], singleref['rank'])
    elif isinstance(self.references, str):
      # upstream file has wrongly formatted reference
      self.log.error("File %s of %s has following references: %s" % (ufile, self.blend, self.references))
    else:
      # upstream file has exactly one reference
      package = ''
      for r in self.references.keys():
        key = r.lower()
        if key != 'debian-package':
          continue
        self.log.warning("Source package '%s' of %s has key 'debian-package'" % (self.source, self.blend))
        package = self.references[r]
      self.setref(self.references, package, 0)
    self.setregistry(self.registry)

    for key in self.fields.keys():
      keyl=key.lower()
      if keyl.startswith('reference-'):
        # sometimes DOI and PMID are stored separately:
        if keyl.endswith('doi'):
          if self.references.has_key('doi') or self.references.has_key('DOI'):
            self.log.warning("Extra key in source package '%s' of %s: %s - please remove from upstream file!" % (self.source, self.blend, key))
            continue
          rdoi={}
          rdoi['rank']    = 0
          rdoi['source']  = self.source
          rdoi['key']     = 'doi'
          rdoi['value']   = re.sub('^doi:', '', re.sub('https?://.*doi.org/', '', self.fields[key]))
          rdoi['package'] = ''          ### Hack!!! we should get rid of Reference-DOI soon to enable specifying 'debian-package' relieable
          self.ubibrefs.append(rdoi)
        elif keyl.endswith('pmid'):
          if self.references.has_key('pmid') or self.references.has_key('PMID'):
            self.log.warning("Extra key in source package '%s' of %s: %s - please remove from upstream file!" % (self.source, self.blend, key))
            continue
          rpmid={}
          rpmid['rank']    = 0
          rpmid['source']  = self.source
          rpmid['key']     = 'pmid'
          rpmid['value']   = self.fields[key]
          rpmid['package'] = ''          ### Hack!!! we should get rid of Reference-PMID soon to enable specifying 'debian-package' relieable
          self.ubibrefs.append(rpmid)
        else:
          print "Source package %s has %s : %s" % (self.source, key, self.fields[key])

  def setref(self, references, package, rank):
    year=''
    defined_fields = { 'address'   : 0,
                       'article'   : 0,
                       'author'    : 0,
                       'booktitle' : 0,
                       'comment'   : 0,
                       'day'       : 0,
                       'debian-package' : 0,
                       'doi'       : 0,
                       'editor'    : 0,
                       'eprint'    : 0,
                       'in'        : 0,
                       'isbn'      : 0,
                       'issn'      : 0,
                       'journal'   : 0,
                       'license'   : 0,
                       'month'     : 0,
                       'number'    : 0,
                       'pages'     : 0,
                       'publisher' : 0,
                       'pmid'      : 0,
                       'title'     : 0,
                       'type'      : 0,
                       'url'       : 0,
                       'volume'    : 0,
                       'year'      : 0,
                     }
    for r in references.keys():
      # print r
      key = r.lower()
      if key == 'debian-package':
        continue
      if defined_fields.has_key(key):
        if defined_fields[key] > 0:
          self.log.error("Duplicated key in source package '%s' of %s: %s" % (self.source, self.blend, key))
          continue
        else:
          defined_fields[key] = 1
      else:
          if key not in ('rank', 'package', 'refid'): # ignore internal maintenance fields
            self.log.warning("Unexpected key in source package '%s' of %s: %s" % (self.source, self.blend, key))
          defined_fields[key] = 1
      ref={}
      ref['rank']    = rank
      ref['source']  = self.source
      ref['key']     = key
      ref['package'] = package
      if isinstance(references[r], int) or isinstance(references[r], float):
        ref['value']   = str(references[r])
      else:
        try:
          ref['value']   = references[r].strip()
        except AttributeError, err:
          if str(references[r]) == 'None':
            self.log.warning("No value given for field '%s' in source package %s of %s - the field is ignored." % (r, self.source, self.blend))
            continue
          else:
            self.log.error("Cannot parse value for source %s of %s: r = %s -> value = %s" % (self.source, self.blend, r, str(references[r])))
            ref['value']   = '???'
        if key == 'author':
          # Try to catch broken author formating
          new_author = re.sub(',\s* and\s*' , ' and ', ref['value'])
          if new_author != ref['value']:
            self.log.warning("Author of source package %s of %s does contain invalid BibTeX format: %s will be turned into %s" % (self.source, self.blend, ref['value'], new_author))
            ref['value'] = new_author
          if ref['value'].count(',') > ref['value'].lower().count(' and ') + 1:
            self.log.warning("Suspicious authors field in source package %s of %s with way more ',' than ' and ' strings: %s" % (self.source, self.blend, ref['value']))
          match = seek_broken_authors_re.search(ref['value'])
          if match:
            self.log.warning("Suspicious authors field in source package %s of %s - seems to have comma separated authors: %s" % (self.source, self.blend, ref['value']))
      self.ubibrefs.append(ref)
      if r.lower() == 'year':
        year = ref['value']
    # Create unique BibTeX key
    bibtexkey = self.source
    if bibtexkey in self.ubibrefsinglelist and year != '':
      bibtexkey = self.source+year
    if bibtexkey in self.ubibrefsinglelist:
      # if there are more than one reference per source package and even in
      # the same year append the rank as letter
      bibtexkey += 'abcdefghijklmnopqrstuvwxyz'[rank]
    ref={}
    ref['rank']    = rank
    ref['source']  = self.source
    ref['key']     = 'bibtex'
    ref['value']   = re.sub('\+', '-', re.sub('\.', '-', bibtexkey)) # avoid '.' and '+' in BibTeX keys
    ref['package'] = package
    self.ubibrefsinglelist.append(bibtexkey)
    self.ubibrefs.append(ref)
    return ref

  def setregistry(self, registry):
    if not registry:
        return None
    defined_fields = { 'name'      : 0,
                       'entry'     : 0,
                     }
    valid_registry = ( 'bio.tools',
                       'biii',
                       'conda:bioconda',
                       'OMICtools',
                       'PyPI', # used in practice but not yet defined in Wiki.  Is this really a registry?
                       'RRID',
                       'SciCrunch',
                       'SEQwiki',
                     )
    
#    print type(registry)
    # fix stupid yaml reader issue
    if isinstance(self.registry, dict):
      reg = []
      reg.append(registry)
    else:
      reg = registry
#    print type(reg)
#    print reg

    return_registry = []
    for r in reg:
      if isinstance(r, str):
        self.log.error("Registry in source %s of %s should not be of type string: %s ... check the isinstance code above!" % (self.source, self.blend, r))
        continue
      if 'Name' not in r:
        self.log.error("No name given for registry in source %s of %s" % (self.source, self.blend))
        continue
      if not r['Name'] in valid_registry:
        self.log.error("Invalid registry in source %s of %s: %s" % (self.source, self.blend, r['Name']))
        continue
      if r['Name'] == 'SEQwiki':
        self.log.warning("Registry in source %s of %s: %s is valid but deprecated" % (self.source, self.blend, r['Name']))
      if not 'Entry' in r:
        self.log.error("Registry entry %s from source %s of blend %s is broken since the 'Entry' key is missing." % (r['Name'], self.source, self.blend))
        continue
      if r['Entry'] == 'NA':
        self.log.info("Registry entry %s from source %s was removed since it is NA." % (r['Name'], self.source))
        continue
      if r['Entry'] == None:
        self.log.error("Registry entry %s from source %s of blend %s was removed since it is empty." % (r['Name'], self.source, self.blend))
        continue
      r['source'] = str(self.source)
      return_registry.append(r)
    self.registry = return_registry


  def get_bibrefs(self):
    return self.ubibrefs

  def get_umetadata(self):
    return self.umetadata

edam_re = re.compile('^ontology:\s+EDAM\s+\(([\d.]+)\)\s*$')

EDAMKEYS   = ['ontology',
              'scopes',
              'topic',
             ]
EDAMTOPICS = ['Bioinformatics',
              'Data management',
              'DNA structural variation',
              'Document, record and content management',
              'Functional, regulatory and non-coding RNA',
              'Gene structure',
              'Genetic variation',
              'Genomics',
              'Mapping',
              'Microbial ecology',
              'Mobile genetic elements',
              'Phylogenetics',
              'Probes and primers',
              'RNA-seq',
              'RNA splicing',
              'Sequence',
              'Sequence analysis',
              'Sequence assembly',
              'Sequence composition, complexity and repeats',
              'Sequencing',
              'Statistics',
              'Structure prediction',
             ]
EDAMSCOPES = ['formats',
              'function',
              'input',
              'name',
              'output',
             ]

def List2PgArray(list):
    # turn a list of strings into the syntax for a PostgreSQL array:
    # {"string1","string2",...,"stringN"}
    if not list:
        return '{}'
    komma='{'
    PgArray=''
    for s in list:
        PgArray=PgArray+komma+'"'+s+'"'
        komma=','
    return PgArray+'}'

class edam_reader():
  """
  Read classifications from single debian/upstream/edam file
  For the definition of this file see
     https://salsa.debian.org/med-team/community/infrastructure/blob/master/edam/README.md
  """

  def __init__(self, efile, source, log, blend):
    self.log = log
    self.source = source
    self.efile = efile
    self.blend = blend
    edam_binary_package_re = re.compile('^.*/'+source+'\.([^.]+)\.edam$')
    match = edam_binary_package_re.match(efile)
    if match:
      self.package = match.group(1)
    else:
      self.package = source
    ef = open(efile, 'r')
    self.erecord='#'
    self.scopes_json = []
    for line in ef.readlines():
      if line.startswith('---'):
        continue
      if self.erecord == '#':
        match = edam_re.match(line)
        if match:
          self.edam_version = match.group(1)
          self.erecord = ''
        else:
          self.log.error("Edam file '%s' of source '%s' of %s has no proper version information (line=%s) --> ignored." % (efile, source, blend, line))
          self.topics = List2PgArray(['FIXME: invalid edam version',])
          return None
      else:
        self.erecord += line
    self.log.debug("Edam record of %s: %s." % (efile, str(self)))

    ef.seek(0, 0)
    try:
      self.fields = yaml.safe_load(ef.read())
    except yaml.scanner.ScannerError, err:
      self.log.error("Scanner error in edam file %s of %s: %s" % (efile, blend, str(err)))
      self.topics = List2PgArray(['FIXME: Scanner error in edam file',])
      return
    except yaml.parser.ParserError, err:
      self.log.error("Parser error in edam file %s of %s: %s" % (efile, blend, str(err)))
      self.topics = List2PgArray(['FIXME: Parser error in edam file',])
      return
    except yaml.reader.ReaderError, err:
      self.log.error("Encoding problem in edam file %s of %s: %s" % (efile, blend, str(err)))
      self.topics = List2PgArray(['FIXME: Encoding problem in edam file',])
      return
    except yaml.composer.ComposerError, err:
      self.log.error("ComposerError in edam file %s of %s: %s" % (efile, blend, str(err)))
      self.topics = List2PgArray(['FIXME: Problem in edam file',])
      return
    # Even string should be encapsulated in a list of size to follow the logic below
    if 'topic' in self.fields:
      if type(self.fields['topic']) is str:
        tmptopic = []
        tmptopic.append(self.fields['topic'])
        self.fields['topic'] = tmptopic
    for key in self.fields.keys():
      if key not in EDAMKEYS:
        self.log.error("Unknown yaml key in Edam file '%s' of source '%s' of %s: %s --> ignored." % (efile, source, blend, key))
        self.fields.pop(key, None)
    for key in self.fields.keys():
      if key == 'ontology':
        continue
      if key == 'topic':
        topic_fields=self.fields['topic']
        topics=[]
        for t in topic_fields:
          if t not in EDAMTOPICS:
            self.log.error("Unknown Edam topic in file '%s' of source '%s' of %s: %s - this will be ignored!." % (efile, source, blend, t))
            continue
          topics.append(t)
        self.topics = List2PgArray(topics)
      if key == 'scopes':
        self.scopes_json=self.fields['scopes'] # list of scopes
        self.scopes = []
        for scope in self.scopes_json:
          scope_dict={}
          for s in scope.keys():
            if s not in EDAMSCOPES:
              self.log.warning("Unknown Edam scope in file '%s' of source '%s' of %s: %s." % (efile, source, blend, s))
            else:
              scope_dict[s]=scope[s]
          self.scopes.append(scope_dict)
    if debug > 0:
      try:
        print "%s::\n  Topics: %s\n  Scopes:" % (self.package, self.topics)
      except AttributeError, err:
        print "%s::\n    Scopes:" % (self.package)
        print "Error: %s" % str(err)
      i = 0
      for scope in self.scopes:
        i+=1
        print "    Scope %i:" % i
        for s in scope.keys():
          print "      %s: %s" % (s, scope[s])
    return

  def get_edam(self):
    edam={}
    edam['source']  = self.source
    edam['package'] = self.package
    try:
      edam['topics']  = self.topics
    except AttributeError:
      self.log.error("Missing Edam topic in file '%s' of source '%s'" % (self.efile, self.source))
      return None
    try:
      edam['scopes']  = json.dumps(self.scopes)
    except AttributeError:
      self.log.error("Missing Edam scopes in file '%s' of source '%s'" % (self.efile, self.source))
      edam['scopes']  = json.dumps([])
      return None
    return edam

  def __str__(self):
    return self.source + ', ' + self.package + ', ' + self.edam_version + '\n ' + self.erecord

class bibtex_check():
  """
  Verify citations by creating BibTeX and LaTeX file
  """

  def __init__(self, cur, log):
    self.bibtexfile = 'debian.bib'
    self.bibtex_example_tex = 'debian.tex'
    self.cur = cur
    self.log = log

  def check(self):
    # if there is a working LaTeX installation try to build a BibTeX database and test it by creating a debian.pdf file
    #print "test whether we can use latex"
#    print "FIXME!!! test is deactivated - reactivate it before pushing!"
#    return
    if isfile('/usr/bin/pdflatex') and access('/usr/bin/pdflatex', X_OK) and \
       isfile('/usr/bin/bibtex')   and access('/usr/bin/bibtex', X_OK) and \
       ( isfile('/usr/share/texlive/texmf-dist/fonts/source/jknappen/ec/ecrm.mf') or \
         isfile('/usr/share/texmf-texlive/fonts/source/jknappen/ec/ecrm.mf') ) :
      # create BibTeX file
      print "latex ok - generate BibTeX file"
      bf = open(self.bibtexfile, 'w')
      self.cur.execute("SELECT * FROM bibtex()")
      for row in self.cur.fetchall():
	print >>bf, row[0]
      bf.close()

      # create LaTeX file to test BibTeX functionality
      bf = open(self.bibtex_example_tex, 'w')
      print >>bf, """\documentclass[10]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[left=2mm,top=2mm,right=2mm,bottom=2mm,nohead,nofoot]{geometry}
\usepackage{longtable}
\usepackage[super]{natbib}
\setlongtables
\\begin{document}
\small
\\begin{longtable}{llp{70mm}l}
\\bf package & \\bf source & \\bf description & BibTeX key \\\\ \hline"""

      self.cur.execute("SELECT * FROM bibtex_example_data() AS (package text, source text, bibkey text, description text)")
      for row in self.cur.fetchall():
	print >>bf, row[0], '&', row[1], '&', row[3] , '&', row[2]+'\cite{'+row[2]+'} \\\\'

      print >>bf, """\end{longtable}

% \\bibliographystyle{plain}
% Try a bit harder by also including URL+DOI
\\bibliographystyle{plainnat}
\\bibliography{debian}

\end{document}
"""
      bf.close()

      # try to build debian.pdf file to test aboc LaTeX file
      basetexfile = self.bibtex_example_tex.replace('.tex','')
      cleanup_tex_logs(basetexfile)
      try:
        rename(basetexfile+'.pdf', basetexfile+'.pdf~')
      except OSError:
        pass

      (retcode,errstring) = open_tex_process('pdflatex', basetexfile)
      if not retcode:
        self.log.error("Problem in 1. PdfLaTeX run of %s.tex: `%s` --> please inspect %s.log" % (basetexfile, errstring, basetexfile))
        exit(1)
      (retcode,errstring) = open_tex_process('bibtex', basetexfile)
      if errstring != "":
        if not retcode:
          self.log.error("Problem in BibTeX run of %s.bib: `%s`" % (basetexfile, errstring))
          exit(1)
        self.log.error("Ignore the following problems in BibTeX run of %s.bib: `%s`" % (basetexfile, errstring))
      (retcode,errstring) = open_tex_process('pdflatex', basetexfile)
      if not retcode:
        self.log.error("Problem in 2. PdfLaTeX run of %s.tex: `%s` --> please inspect %s.log" % (basetexfile, errstring, basetexfile))
        exit(1)
      (retcode,errstring) = open_tex_process('pdflatex', basetexfile)
      if not retcode:
        self.log.error("Problem in 3. PdfLaTeX run of %s.tex: `%s` --> please inspect %s.log" % (basetexfile, errstring, basetexfile))
        exit(1)

      cleanup_tex_logs(basetexfile)
    else:
      print "No generation of BibTeX file since there is no LaTeX installation which is OK"
      return True

if __name__ == '__main__':
  main()

# vim:set et tabstop=2:
