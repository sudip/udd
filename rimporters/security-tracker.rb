#!/usr/bin/ruby

def update_security_tracker
  db = PG.connect(UDD_USER_PG)
  db.exec("BEGIN")
  db.exec("SET CONSTRAINTS ALL DEFERRED")
  db.exec("DELETE FROM security_issues")
  db.exec("DELETE FROM security_issues_releases")

  db.prepare('issues', "insert into security_issues values ($1, $2, $3, $4, $5)")
  db.prepare('releases', "insert into security_issues_releases values ($1, $2, $3, $4, $5, $6, $7)")

  d = JSON::parse(open('https://security-tracker.debian.org/tracker/data/json').read)
  allinfokeys = []
  allrelinfokeys = []
  d.each_pair do |source, issues|
    issues.each_pair do |issue, info|
      allinfokeys << info.keys
      db.exec_prepared('issues', [source, issue, info['description'], info['scope'], info['debianbug']])
      info['releases'].each_pair do |release, relinfo|
        allrelinfokeys << relinfo.keys
        db.exec_prepared('releases', [source, issue, release, relinfo['fixed_version'], relinfo['status'], relinfo['urgency'], relinfo['nodsa']])
      end
    end
  end
  db.exec("COMMIT")

  # Sanity checks
  unknowninfokeys = allinfokeys.flatten.uniq - ["releases", "description", "scope", "debianbug"]
  if not unknowninfokeys.empty?
    raise "Unknown info key: #{unknowninfokeys}"
  end
  unknownrelinfokeys = allrelinfokeys.flatten.uniq - ["fixed_version", "repositories", "status", "urgency", "nodsa", "nodsa_reason"]
  if not unknownrelinfokeys.empty?
    raise "Unknown relinfo key: #{unknownrelinfokeys}"
  end
end
