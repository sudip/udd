def update_duck
  db = PG.connect({ :dbname => 'udd', :port => 5452})

  db.exec('BEGIN')
  # FIXME there might be more suites at some point
  packages = open('http://duck.debian.net/sourcepackages.txt').readlines.map { |l| l.chomp }
  db.exec('DELETE FROM duck')
  db.prepare('duck_insert', 'INSERT INTO duck (source) VALUES ($1)')
  packages.each do |pkg|
    db.exec_prepared('duck_insert', [ pkg ] )
  end
  db.exec("COMMIT")
end
