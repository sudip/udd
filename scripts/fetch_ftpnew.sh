#!/bin/sh
TARGETDIR=/srv/udd.debian.org/mirrors/ftpnew
mkdir -p $TARGETDIR
rm -rf $TARGETDIR/*

# work around a regression in wget from wheezy to jessie
# see https://wiki.debian.org/ServicesSSL#wget
WGET=wget
dir=/etc/ssl/ca-debian
test -d $dir && WGET="wget --ca-directory=$dir"

$WGET -q http://ftp-master.debian.org/new.822 -O ${TARGETDIR}/new.822
cd $TARGETDIR
for newhtml in `$WGET -q -O- http://ftp-master.debian.org/new.html | grep '^<a href="new/.*\.html' | sed 's?^<a href="\(new/.*\.html\).*?http://ftp-master.debian.org/\1?'` ; do
    $WGET -q $newhtml
    # very rude hack to cover cases with multi-line fields specifying Binary packages
    # the correct way to deal with this would probably be to rewrite the importer using beautifisoup
    htmlfile=`echo $newhtml | sed 's:^.*/\([^/]\+\.html\)$:\1:'`
    sed -i '/<tr><td class="key">Binary:<\/td><td class="val">.*[A-Za-z]$/{;N;s/\n//g;}' $htmlfile
    sed -i '/<tr><td class="key">Binary:<\/td><td class="val">.*[A-Za-z]$/{;N;s/\n//g;}' $htmlfile
    sed -i '/<tr><td class="key">Binary:<\/td><td class="val">.*[A-Za-z]$/{;N;s/\n//g;}' $htmlfile
    sed -i '/<tr><td class="key">Binary:<\/td><td class="val">.*[A-Za-z]$/{;N;s/\n//g;}' $htmlfile
done
## Access to webdir is forbidden since 2015-05-20:
## https://lists.debian.org/debian-qa/2015/05/msg00032.html
# wget -q -r -N --level=2 --no-parent --no-directories http://ftp-master.debian.org/new/
