#!/usr/bin/ruby
# encoding: utf-8
Encoding.default_internal = 'UTF-8'

# See #647258

$:.unshift('../rlibs')
require 'udd-db'
require 'pp'
require 'cgi'
require 'time'
require 'yaml'
require 'json'
require 'open-uri'
puts "Content-type: text/yaml\n\n"

rgs = open("https://release.debian.org/testing/goals.yaml").read
rgs = YAML::load(rgs)
rgs = rgs['release-goals']
# only rgs that are not rejected or proposed
rgs = rgs.select { |rg| rg['state'] != 'rejected' }
rgs = rgs.select { |rg| rg['state'] != 'proposed' }
# only rgs that have associated usertags
rgs = rgs.select { |rg| rg['bugs'] != nil }
usertags = []
rgs.each do |rg|
  rg['bugs']['usertags'].each do |ut|
    usertags << [ rg['bugs']['user'], ut ]
  end
end
if !usertags.empty?
  ut_text = usertags.map { |e| "('#{e[0]}', '#{e[1]}')"}.join(',')
  DB = Sequel.connect(UDD_GUEST)
  DB["SET statement_timeout TO 90000"]
  q = "select distinct bugs.id, bugs.source, bugs.package, bu.email, bu.tag from bugs, bugs_usertags bu where bugs.id = bu.id and status = 'pending' and (bu.email, bu.tag) in (#{ut_text})"
  rows = DB[q].all.sym2str
  rows = rows.map { |r| r.to_h }
  puts YAML::dump(rows)
end
