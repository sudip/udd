#!/usr/bin/python3
# -*- coding: utf-8; mode: python; tab-width: 4; -*-

"""
List package autoremovals in YAML format.
"""

import sys
import os
sys.path.insert(0, os.path.abspath('../../pylibs/'))
from cgi_helpers import *
import yaml
from psycopg2 import connect
from psycopg2.extras import DictCursor
from datetime import datetime


DATABASE = 'service=udd'
QUERY = """\
  SELECT *
    FROM testing_autoremovals
ORDER BY first_seen
"""

print_contenttype_header('text/plain')

conn = connect(DATABASE)
cur = conn.cursor(cursor_factory=DictCursor)
cur.execute(QUERY)
rows = cur.fetchall()
cur.close()
conn.close()

data = {}

for row in rows:
    entry = {}
    entry['source'] = row['source']
    entry['version'] = row['version']
    if row['bugs'] == "":
        entry['bugs'] = []
        entry['dependencies_only'] = True
    else:
        entry['bugs'] = row['bugs'].split(",")
        entry['dependencies_only'] = False
    if row['rdeps'] != "":
        entry['rdeps'] = row['rdeps'].split(",")
    if row['buggy_deps'] != "":
        entry['buggy_dependencies'] = row['buggy_deps'].split(",")
    if row['bugs_deps'] != "":
        entry['bugs_dependencies'] = row['bugs_deps'].split(",")
    entry['last_checked'] = datetime.utcfromtimestamp(row['last_checked'])
    entry['removal_date'] = datetime.utcfromtimestamp(row['removal_time'])
    data[row['source']] = entry

print(yaml.dump(data,
                explicit_start=True,
                default_flow_style=False))
